public class Circle {
    private Point center;
    private double radius;

    public Circle(Point center, double radius) {
        this.center = center;
        this.radius = radius;
    }

    boolean containsPoint(Point p) {
        return p.distanceTo(this.center) < this.radius;
    }
}